## JavaFX系列框架项目导航
- [JDK8 & JavaFX & SpringBoot](https://gitee.com/westinyang/javafx-boot) `加持SpringBoot，项目示例，Maven打包插件带可执行程序`
- [JDK8 & JavaFX](https://gitee.com/westinyang/javafx-start) `不依赖SpringBoot，项目示例，Maven打包插件带可执行程序`
- **[JDK11+ & JavaFX15](https://gitee.com/westinyang/javafx-jdk11-start)**  
    - `使用 jlink 打包为精简版jvm，7z压缩后约16m左右`
    - `使用 GraalVM native-image 静态编译不依赖jvm，7z压缩后13m左右`
    
---

![LOGO](./src/main/resources/com/ceadeal/javafxjdk11start/icon/icon.png)

# 介绍
JavaFx15 & JDK11+ 快速开始项目模板（模块化）

由于JDK11+ JavaFx SDK 独立出来，需要手动配置或使用Maven依赖安装，有别于JDK1.8内置的JavaFx。

使用 `jlink` 打包为精简版jvm，7z压缩后16m左右。

使用 `GraalVM native-image` 静态编译不依赖jvm，7z压缩后13m左右，运行速度和内存占用大幅提升。

> 参考资料：https://openjfx.cn/openjfx-docs

体验发行版 [点此下载](https://gitee.com/westinyang/javafx-jdk11-start/releases)

# jlink 打包
- Maven插件 [openjfx/javafx-maven-plugin](https://github.com/openjfx/javafx-maven-plugin)

- 注意事项：pom.xml中有如下配置，我本机依然使用的是JDK1.8，所以指定了编译时的JDK。
```pom
...
<!-- 如果JAVA_HOME环境变量已经设置为11或者更高，则不需要配置executable标签 -->
<executable>D:/dev/jdk-11.0.10/bin/java</executable>
...
```
- 打包minijvm
```cmd
mvn javafx:jlink
```
- 启动应用
    - linux/mac `./target/javafx-jdk11-start/bin/launcher`
    - win `.\target\javafx-jdk11-start\bin\launcher`

# GraalVM native-image 打包

> 
> 依赖 `JDK 11 +` `GraalVM 21.0.0 +` `Visual Studio 2019` ...    
> 参考 Gluon官网文档 https://docs.gluonhq.com/#platforms_windows  
> 参考 网友博客文档 https://www.cnblogs.com/dehai/p/14258391.html

- Maven插件 [gluonhq/client-maven-plugin](https://github.com/gluonhq/client-maven-plugin)

- 打包命令（下面的VS路径更改成你自己的，社区版也可以）
```cmd
call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\VC\Auxiliary\Build\vcvars64.bat"
mvn client:build
```

- 启动应用 `.\target\client\x86_x64-windows\javafx-jdk11-start.exe`

# 项目配置（app.properties）

```properties
title=JavaFx JDK11 Start
icon=icon/icon.png
stage.width=640
stage.height=440
stage.resizable=false
```

# 项目截图
![截图](./screenshot/01.png)

# 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
